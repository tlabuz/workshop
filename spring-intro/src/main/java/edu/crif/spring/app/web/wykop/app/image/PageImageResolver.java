package edu.crif.spring.app.web.wykop.app.image;

public interface PageImageResolver {

    String resolveImageURL(String pageURL);


}
