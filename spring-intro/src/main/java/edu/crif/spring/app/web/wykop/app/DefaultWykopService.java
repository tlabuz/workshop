package edu.crif.spring.app.web.wykop.app;

import edu.crif.spring.app.web.wykop.domain.Wykop;
import edu.crif.spring.app.web.wykop.infrastructure.WykopRepository;
import edu.crif.spring.app.web.wykop.webapp.CreateWykopRequest;

import java.util.List;


public class DefaultWykopService implements WykopService {

  private WykopFactory wykopFactory;


  private WykopRepository wykopRepository;

  public DefaultWykopService(WykopFactory wykopFactory,
      WykopRepository wykopRepository) {
    this.wykopFactory = wykopFactory;
    this.wykopRepository = wykopRepository;
  }

  @Override
  public void createWykop(CreateWykopRequest request) {

  }

  @Override
  public List<Wykop> selectAll() {
    return wykopRepository.findAll();
  }
}
