package edu.crif.spring.app.web.wykop.infrastructure;

import edu.crif.spring.app.web.wykop.domain.Wykop;

import java.util.ArrayList;
import java.util.List;


public class InternalWykopRepository {

  private List<Wykop> list = new ArrayList<>();

  public void save(Wykop wykop) {
    list.add(wykop);
  }

  public List<Wykop> findAll() {
    return new ArrayList(list);
  }
}
