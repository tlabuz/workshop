package edu.crif.spring.app.web.wykop.app;

import edu.crif.spring.app.web.wykop.domain.Wykop;

import edu.crif.spring.app.web.wykop.webapp.CreateWykopRequest;
import java.util.List;

public interface WykopService {

    void createWykop(CreateWykopRequest request);

    List<Wykop> selectAll();

}
